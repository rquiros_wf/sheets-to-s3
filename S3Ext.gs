/* deletes bucket from S3 
 *
 * @param {string} bucket name of bucket
 * @param {Object} options optional parameters to delete request
 * @throws {Object} AwsError on failure
 * @return void
 */
S3.S3.prototype.getBucket = function (bucket, options) {
  options = options || {};

  var request = new S3.S3Request(this);
  request.setHttpMethod('GET');
  
  request.setBucket(bucket);
  try {
    var responseBlob = request.execute(options).getBlob();
  } catch (e) {
    if (e.name == "AwsError" && e.code == 'NoSuchKey') {
      return null;
    } else {
      //some other type of error, rethrow
      throw e; 
    }
  }
  
  // parse xml
  var xml = responseBlob.getDataAsString();
  var ns = XmlService.getNamespace("http://s3.amazonaws.com/doc/2006-03-01/");
  var document = XmlService.parse(xml);
  var root = document.getRootElement();
  var entries = document.getRootElement().getChildren("Contents", ns);
  
  var list = [];
  for (var i = 0; i < entries.length; i++) {
    var entry = {
      key: entries[i].getChildText("Key", ns),
      lastModified: new Date(entries[i].getChildText("LastModified", ns)),
    };
    list.push(entry);
  }
  
  return list;
};

S3.S3.prototype.getObjectAcl = function(bucket, objectName, options) {
  options = options || {};
  
  var request = new S3.S3Request(this);
  request.setHttpMethod('GET');
  
  request.setBucket(bucket);
  request.setObjectName(objectName + "?acl");
  try {
    var responseBlob = request.execute(options).getBlob();
  } catch (e) {
    if (e.name == "AwsError" && e.code == 'NoSuchKey') {
      return null;
    } else {
      //some other type of error, rethrow
      throw e; 
    }
  }
  
  
  // parse xml
  var acl = {};
  var xml = responseBlob.getDataAsString();
  var ns = XmlService.getNamespace("http://s3.amazonaws.com/doc/2006-03-01/");
  var document = XmlService.parse(xml);
  var root = document.getRootElement();
  
  var granteeNS = XmlService.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
  
  var owner = root.getChild("Owner", ns);
  acl.owner = {
    id: owner.getChildText("ID", ns),
    displayName: owner.getChildText("DisplayName", ns)
  };
  acl.accessControlList = root.getChild("AccessControlList", ns).getChildren("Grant", ns)
    .map(function(grant) {
      var grantee = grant.getChild("Grantee", ns);
      return {
        id: grantee.getChildText("ID", ns),
        displayName: grantee.getChildText("DisplayName", ns),
        permission: grant.getChildText("Permission", ns)
      };
    });
  return acl;
};

function testGetBucket() {
  var props = PropertiesService.getScriptProperties().getProperties();
  var message = "";
  var s3 = S3.getInstance(props.awsAccessKeyId, props.awsSecretKey);
  Logger.log(s3.getBucket(props.bucketName));
}

function testGetObjectAcl() {
  var s3 = S3.getInstance("AKIAIXYWADCTUF6HSF5A", "cN3gw+NeQLXorisg4UpWVnDeIw/FHJpnTYsXtHbE");
  Logger.log(JSON.stringify(s3.getObjectAcl("psad-game-data", "dev/roland_quiros/item_data_trunk.json"), null, 2));
}