function testPublishAll() { publish("asdf", "item"); }

function publish(filename, group) {
  if (!filename) { throw new Error("No filename provided!"); } 
  
  // prepend the config prefix to the filename
  var s3Config = PropertiesService.getUserProperties().getProperties().s3Config;
  s3Config = s3Config ? JSON.parse(s3Config) : {};
  filename = s3Config.objectPrefix + filename;
  
  var json = {};
  if (group) {
    var sheets = PropertiesService.getScriptProperties().getProperties().sheets;
    if (sheets) {
      var allSheets = JSON.parse(sheets);
      var groupSheets = allSheets[group];
      var ids = Object.keys(groupSheets).map(function(key) {
        return groupSheets[key];
      });
      
      json = ids.reduce(function(allData, id) {
        var file = DriveApp.getFileById(id); 
        var spreadsheet = SpreadsheetApp.open(file);
        return assign(allData, Parse.standard(spreadsheet));
      }, {});
    } else {
      throw new Error("No listing of sheets have been defined!");
    }
  } else {
    var spreadsheet = SpreadsheetApp.getActive();
    // TODO: Check file permission before processing the JSON
    // Parse the sheet and publish it to S3
    json = Parse.standard(spreadsheet);
  }
  publishToS3(json, filename);
}

var publishToS3 = function(obj, filename) {
  // Send data to S3
  var s3Config = PropertiesService.getUserProperties().getProperties().s3Config;
  if (s3Config) {
    props = JSON.parse(s3Config);
    var s3 = S3.getInstance(props.awsAccessKeyId, props.awsSecretKey);
    s3.putObject(props.bucketName, filename, obj);
    
    var ui = SpreadsheetApp.getUi();
    ui.alert(
      '✓ Publish complete',
      "Item data successfully pushed to \nhttps://" + props.bucketName + ".s3.amazonaws.com/" + filename,
      ui.ButtonSet.OK
    );
  } else {
    ui.alert("Your S3 target has not been configured! Set up your connection in Publish to S3 > Configure...", ui.ButtonSet.OK);
  }
}