PLAYFAB_DEFAULTS = {
  CanBecomeCharacter: false,
  InitialLimitedEditionCount: 0,
  IsLimitedEditionCount: 0,
  IsLimitedEdition: false,
  IsTradable: false
};

var playfabProperties = [
  "ItemId",
  "ItemClass",
  "DisplayName",
  "Description",
  "VirtualCurrencyPrices",
  "Consumable",
  "CanBecomeCharacter",
  "IsStackable",
  "IsTradable",
  "IsLimitedEdition",
  "InitialLimitedEditionCount"
];

var extractPlayfabProperties = function (item) {
  return Object.keys(item)
  .filter(function(k) { return item.includes(k); })
  .reduce(function(obj, key) {
    return obj[key] = item[key];
  }, {});
}

function pushToPlayfab(items, catalogVersion) {
  var catalog = items.reduce(function(catalogItems, item) { return extractPlayfabProperties(item); });
  
  var data = {
    CatalogVersion: catalogVersion,
    Catalog: catalog,
    SetAsDefaultCatalog: false
  };
  var options = {
    method: "post",
    contentType: "application/json",
    headers: {
      "X-SecretKey":"RSCETD1WOKMDRGEU3HSG1KD6NWDRGKFSNQSF56KOFIO9YCUYNP"
    },
    payload: JSON.stringify(data)
  };
  
  UrlFetchApp.fetch(url, params);
}