/**
* checks if document has the required configuration settings to publish to S3
* does not check if the config is valid
*/
var hasRequiredProps = function () {
  var props = PropertiesService.getUserProperties().getProperties().s3Config;
  if (props) {
    props = JSON.parse(props);
    return props.bucketName && props.awsAccessKeyId && props.awsSecretKey;
  }
  return false;
}

function createPublishMenu() {
  var ui = SpreadsheetApp.getUi();
  var menu = ui.createMenu("Publish to S3");
  
  var userProps = PropertiesService.getUserProperties().getProperties();
  
  menu
  .addItem("Publish As...", "PSData.showPublish")
  .addItem("Preview JSON", "PSData.showPreview")
  .addSeparator()
  .addItem("Publish Items...", "PSData.showPublishItems")
  .addSeparator()
  .addItem("Configure...", "PSData.showConfig")
  .addToUi();
}

/**
* Displays the "Publish As..." modal dialog
*/
function showPublish(group) {
  var ui = SpreadsheetApp.getUi();
  var template = HtmlService.createTemplateFromFile("publish");
  var s3Config = PropertiesService.getUserProperties().getProperties().s3Config;
  if (s3Config && hasRequiredProps()) {
    s3Config = JSON.parse(s3Config);
    var s3 = S3.getInstance(s3Config.awsAccessKeyId, s3Config.awsSecretKey);
    var prefixPattern = new RegExp("^" + s3Config.objectPrefix);

    template.group = group;
    template.files = s3.getBucket(s3Config.bucketName)
      .filter(function(f) { return f.key != s3Config.objectPrefix && prefixPattern.test(f.key); })
      .map(function(f) { return { key: f.key.replace(prefixPattern, ""), lastModified: f.lastModified}; });
      
    ui.showModalDialog(
      template.evaluate().setWidth(600).setHeight(450),
      "Publish As..."
    );
  } else {
    ui.alert("No publishing configuration exists! Set up your connection in Publish to S3 > Configure...", ui.ButtonSet.OK);
  }
}

/**
 * Displays the Publish Items dialog
 */
function showPublishItems() {
  var ui = SpreadsheetApp.getUi();
  var yn = ui.alert("This will publish the data from all item data sheets into a single file. This may take a long time. Continue?", ui.ButtonSet.YES_NO);
  if (yn == ui.Button.YES) {
    showPublish("item");
  }
}

/**
 * Displays the JSON output of the currently active spreadsheet
 */
function showPreview() {
  try {
    var ui = SpreadsheetApp.getUi();
    var errors = [];
    var json = Parse.standard(SpreadsheetApp.getActiveSpreadsheet(), errors);
    
    var template = HtmlService.createTemplateFromFile("preview");
    template.previewBody = JSON.stringify(json, null, 4);
    template.errors = errors;
    ui.showModalDialog(template.evaluate().setHeight(420), 'JSON Output Preview');
  } catch (e) {
    throw new Error(e.fileName + ":" + e.lineNumber + " - " + e.message);
  }
}

/**
 * Show configuration dialog
 */
function showConfig() {
  var ui = SpreadsheetApp.getUi();
  
  var userS3Config = PropertiesService.getUserProperties().getProperties().s3Config;
  userS3Config = userS3Config ? JSON.parse(userS3Config) : {};
  
  var presets = JSON.parse(PropertiesService.getScriptProperties().getProperties().s3ConfigPresets);
  var template = HtmlService.createTemplateFromFile("config");
  
  template.presets = [{name: ""}].concat(presets);
  template.bucketName = userS3Config.bucketName || "";
  template.objectPrefix = userS3Config.objectPrefix || "";
  template.awsAccessKeyId = userS3Config.awsAccessKeyId || "";
  template.awsSecretKey = userS3Config.awsSecretKey || "";
  ui.showModalDialog(template.evaluate(), 'Amazon S3 publish configuration');
}

/**
 * Update user configuration with form values
 */
function updateConfig(form) {
  var sheet = SpreadsheetApp.getActiveSpreadsheet();
  PropertiesService.getUserProperties().setProperties({
    s3Config: JSON.stringify({
      bucketName: form.bucketName,
      objectPrefix: form.objectPrefix,
      awsAccessKeyId: form.awsAccessKeyId,
      awsSecretKey: form.awsSecretKey
    })
  });
  var ui = SpreadsheetApp.getUi();
  
  Utilities.sleep(3000); // Gotta wait for the stupid PropertiesService to actually commit changes
  ui.alert('✓ Configuration updated successfully!', ui.ButtonSet.OK);
}