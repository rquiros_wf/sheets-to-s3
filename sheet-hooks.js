function authAndRun() {
  ScriptApp.getAuthorizationInfo(ScriptApp.AuthMode.FULL);
  PSData.createPublishMenu();
}
function onInstall() { authAndRun(); }
function onOpen() { authAndRun(); }
function updateConfig(form) { PSData.updateConfig(form); }
function publish(filename, group) { PSData.publish(filename, group); }