var extractIndex = function(token) {
  var matches = /\[(.*)\]$/.exec(token);
  var index = null;
  if (matches && matches.length > 0) {
    index = isNumeric(matches[1]) ? parseFloat(matches[1]) : matches[1];
  }
  return index;
}

var removeIndex = function(token) {
  return token.replace(/\[.*\]$/, "");
}

JSONPath = {
  /**
   * Converts a data range into an array of JSONPaths.
   * Each column represents a field
   * Each row represents a level of the tree
   */
  collapseMetacolumns: function(range) {
    var previousRoots = new Array(range.length);
    var paths = [];
    // Move down the column rows
    // If a cell is blank, use the previous key in that row
    for (var c = 0; c < range[0].length; c++) {
      var tokens = [];
      var skip = false;
      for (var r = 0; !skip && r < range.length; r++) {
        var cell = range[r][c] || previousRoots[r];
        if (cell) {
          cell = cell.trim();
          previousRoots[r] = cell;
          if (cell == "null") { // If a cell is marked "null", we ignore the entire column and add an empty path
            tokens.length = 0;
            break;
          } else {
            tokens.push(cell);
          }
        }
      }
      // Construct a JSON Path string
      var path = "";
      if (tokens.length > 0) {
        path = (tokens[0] && tokens[0][0] !== "$" ? "$." : "") + tokens.join(".");
      }
      paths.push(path);
    }
    return paths;
  },
  insert: function(target, path, value) {
    var tokens = path.split(".");
    if (tokens[0][0] != "$") {
      throw new Error("JSONPaths must start with $ (" + path + ")");
    }
    
    var newObj = assign({}, target);
    if (target instanceof Array) {
      newObj = { "$": target.slice() };
    } else {
      newObj = { "$": assign({}, target) };
    }
    
    var current = newObj;
    for (var t = 0; t < tokens.length; t++) {
      var token = tokens[t];
      if (token == "") {
        //throw new Error("A token in path \"" + path + "\" resolved to an empty string");
      }
      
      var index = extractIndex(token);
      token = removeIndex(token);
      
      if (t === tokens.length - 1) {
        current[token] = value;
      } else {
        // If the current key doesn't exist in the object, create it
        if (!(token in current)) {
          current[token] = isNumeric(index) ? new Array(index + 1) : {};
        }
        // If the current token has an index, point to the object at that index
        if (isNumeric(index)) {
          if (!(index in current[token])) {
            current[token][index] = {};
          }
          current = current[token][index];
        } else {
          current = current[token];
        }
      }
    }
    
    return newObj["$"];
  },
  extract: function(target, path) {
    var tokens = path.split(/[\.\[\]]/);
    if (tokens[0] != "$") {
      throw new Error("JSONPaths must start with $");
    }
    
    var current = target;
    tokens.slice(1).map(function(token) {
      current = current[token];
    });
    
    return current;
  }
};