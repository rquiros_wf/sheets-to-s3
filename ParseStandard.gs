var safeEval = function(str, ROWNUM, ROW, ROWS, RUNROWS) {
  // Search the previously processed rows for occurrences of the current row's column value
  var COUNT = function(headers) {
    if (!(headers instanceof Array)) {
      headers = [headers];
    }
    var search = headers.reduce(function(s, h) { s[h] = ROW[h]; return s; }, {});
    return RUNROWS.reduce(function(count, row) {
      if (headers.some(function(h) { return row[h] != search[h]; })) {
        return count;
      } else {
        return count + 1;
      }
    }, 0);
  };
  // Count the current row's value in the entire dataset 
  var TOTAL = function(headers) {
    if (!(headers instanceof Array)) {
      headers = [headers];
    }
    var search = headers.reduce(function(s, h) { s[h] = ROW[h]; return s; }, {});
    return ROWS.reduce(function(count, row) {
      if (headers.some(function(h) { return row[h] != search[h]; })) {
        return count;
      } else {
        return count + 1;
      }
    }, 0);
  };
  
  return eval(str);
};

var rowObject = function(headers, row) {
  return row.reduce(function(obj, col, colIndex) {
    obj[headers[colIndex]] = col;
    return obj;
  }, {});
}

Parse = {
  standard: function(spreadsheet, errors) {
    var sheets = spreadsheet.getSheets().filter(function(sheet) { return !sheet.isSheetHidden() && !sheet.getName().startsWith("_"); })
    
    var json = sheets.reduce(function(allData, sheet) {
      // Get the JSON paths for each column
      var ranges = sheet.getNamedRanges().filter(function(range) { return range.getName() == "_" + sheet.getName().replace(/\s/g, ""); });
      if (ranges.length <= 0) {
        throw new Error("No meta range defined for sheet " + sheet.getName());
      }
      var metaRange = ranges[0].getRange();
      var paths = JSONPath.collapseMetacolumns(metaRange.getValues());
      
      // Parse the rows, omitting the metadata and headers
      var values = sheet.getDataRange().getValues().slice(metaRange.getLastRow());
      var headers = values[0];
      var data = values.slice(1);
      
      // Turn all rows into an array of objects
      var ROWS = data.map(function(row) { return rowObject(headers, row);});
      var RUNROWS = [];
      
      data.forEach(function(row, rowIndex) {
        // Injection helpers
        var ROW = ROWS[rowIndex];
        var ROWNUM = rowIndex;
        RUNROWS.push(ROW);
        
        for (var c = 0; c < row.length; c++) {
          try {
            if (row[c] === "delete") {
              var x = 0;
            }
            if (checkNull(row[c]) == null || checkNull(paths[c]) == null) { continue; }
            var originalPath = paths[c];
            var path = originalPath;
            var injectionPattern = /{(.*?)}/g;
            var injections;
            while (injections = injectionPattern.exec(originalPath)) {
              var eval = safeEval(injections[1], ROWNUM, ROW, ROWS, RUNROWS);
              path = path.replace(injections[0], eval);
            }
            if (path) {
              allData = JSONPath.insert(allData, path, row[c]);
            }
          } catch(e) {
            if (errors instanceof Array) {
              errors.push({
                rowNumber: metaRange.getLastRow() + rowIndex,
                sheet: sheet.getName(),
                error: e
              });
            }
          }
        }
      });
      return allData;
    }, {});
    
    return json;
  }
};

function testParseWeapons() {
  var file = DriveApp.getFileById("1H3RlOWl5IDhBG7c33UpSkO_aFjt-QxY9eG4IWBGNmw4"); 
  var spreadsheet = SpreadsheetApp.open(file);
  var errors = [];
  var json = Parse.standard(spreadsheet, errors);
  Logger.log(JSON.stringify(json, null, 2));
  Logger.log(JSON.stringify(errors, null, 2));
}